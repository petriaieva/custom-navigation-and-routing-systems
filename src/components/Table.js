function Table({ data, config, keyFn }) {
    const renderedFruits = data.map((rowData) => {
        const renderedCells = config.map((column) => {
            return <td key={column.label}>{column.render(rowData)}</td>
        });
        return (
            <tr key={keyFn(rowData)} className='border-b'>
               {renderedCells}
            </tr>
        );
    }

    );

    const renderedLabels = config.map((column) => {
        return <th key={column.label}>{column.label}</th>
       
    
    }

    );

    return <table className="table-auto border-spacing-2">
        <thead>
            <tr className="border-b-2">
                {renderedLabels}
            </tr>
        </thead>
        <tbody>
            {renderedFruits}
        </tbody>
    </table>
}

export default Table;