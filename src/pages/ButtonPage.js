import Button from "../components/Button";
import { FaYoutube, FaInstagram, FaFacebookSquare } from "react-icons/fa";


function ButtonPage() {
const handleClick = () => {
  
}
const handleMouseOver = () => {
   console.log('On mouse over!');
}

    return <div>
        <div>
            <Button success className="mb-5" rounded outline onClick={handleClick}><FaYoutube />Click me!!</Button>
         </div>
         <div>
            <Button danger onMouseOver={handleMouseOver} outline><FaInstagram />Buy now!</Button>
         </div>
         <div>
            <Button warning>See Deal!</Button>
         </div>
         <div>
            <Button secondary outline><FaFacebookSquare /> Hide Ads!</Button>
         </div>
         <div>
            <Button primary rounded>Something!</Button>
         </div>
         
    </div>;
}

export default ButtonPage;