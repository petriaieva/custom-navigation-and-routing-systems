import { useState, useEffect, useRef } from "react";
import { GoChevronDown } from "react-icons/go";
import Panel from "./Panel";

function Dropdown({ options, value, onChange}) {
    const [isOpen, setIsOpen] = useState(false);
    const divEl = useRef();

    useEffect(() => {
        //Runs only on the first render
        const handler = (ev) => {
            if (!divEl.current) {return;}
            
            if (!divEl.current.contains(ev.target)) {setIsOpen(false);}
        }

        document.addEventListener("click", handler, true);
        return () => {
            document.removeEventListener("click", handler);
        };
      }, []);

    const handleClick = () => {
        onChange(null);
        setIsOpen(!isOpen);
    }

    const handleOptionClick = (opt) => {
        setIsOpen(false);
        onChange(opt);
    }

    const renderedOptions = options.map((option) => {
        return <div className="hover:bg-sky-100 rounded cursor-pointer p-1"onClick={() => handleOptionClick(option)} key={option.value}>
            {option.label}
        </div>
    });

    // let content = "Select...";
    // if (selection) {
    //     content = selection.label;
    // }

    return (
        <div ref={divEl} className="w-48 relative">
           
                <Panel className="flex justify-between items-center cursor-pointer" onClick={handleClick}>
                    {value?.label || "Select..."} <GoChevronDown className="text-lg" /></Panel>
           
            { isOpen && (<Panel className="absolute top-full">{renderedOptions}</Panel>)}
        </div>
    );
   
}

export default Dropdown;