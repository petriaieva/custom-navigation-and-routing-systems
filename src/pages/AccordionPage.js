import Accordion from "../components/Accordion";



function AccordionPage() {
   const items = [
      {
         id: "fjhi7",
         label: "Can I use React on a project?",
         content: "You can use React on any project you want."
      },
      {
         id: "fjhi8",
         label: "Can I use JavaScript on a project?",
         content: "Yes of caurse."
      },
      {
         id: "fjhi9",
         label: "Can I use CSS on a project?",
         content: "More."
      },
   ]


   return <Accordion items={items}></Accordion>;
}

export default AccordionPage;