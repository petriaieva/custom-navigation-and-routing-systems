import { useState } from "react";
import Dropdown from "../components/Dropdown";

function DropDownPage() {
   const options = [
      {label: "Red", value: "red"},
      {label: "Green", value: "green"},
      {label: "Blue", value: "blue"},
   ];

   const [selection, setSelection] = useState(null);

   const handleSelect = (option) => {
      setSelection(option);
   }

  

   return <div className="flex">
         <Dropdown value={selection} onChange={handleSelect} options = {options}/>
         
      </div>;
}

export default DropDownPage;